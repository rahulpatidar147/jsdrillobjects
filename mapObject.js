function mapObject(obj,cb){
let resultObject={};
for(let value in obj){
    resultObject[value]=cb(obj[value]);
} 
return resultObject;
}
module.exports=mapObject;