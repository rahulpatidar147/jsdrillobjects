function defaults(obj, defaultProps){
for(let property in defaultProps){
    if(obj[property]===undefined){
        obj[property]=defaultProps[property];
    }
}
  return obj;
}
module.exports=defaults;
